import xml.etree.ElementTree as ET
"""Script to generate role defaults game settings and template serverconfig.xml.j2 from serverconfig.xml
"""

def main():
    parser = ET.XMLParser(target=ET.TreeBuilder(insert_comments=True))
    tree = ET.parse('files/serverconfig.xml', parser)
    root = tree.getroot()

    for child in root:
        if child.attrib.get("name") is None:
            continue
        var_name = f'sdtd_gs_{child.attrib.get("name").lower()}'
        print(f'{var_name}: "{child.attrib.get("value")}"')
        child.set("value", f'{{{{ { var_name } }}}}')

    tree.write('templates/serverconfig.xml.j2')


if __name__ == '__main__':
    main()
