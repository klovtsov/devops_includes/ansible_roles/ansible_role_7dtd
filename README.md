Ansible Role: 7 Days to Die Server
=========

Install and configure 7 days to die game server

Requirements
------------

None

Role Variables
--------------

Deafults
[defaults/main.yml](defaults/main.yml)

Dependencies
------------

[ansible_role_steamcmd](https://gitlab.com/klovtsov/devops_includes/ansible_roles/ansible_role_steamcmd)

Example Playbook
----------------

    - name: Install 7 days to die game server
      hosts: gameserver
      become: true
      gather_facts: true
      vars:
        steamcmd_user: "7dtd"
        steamcmd_group: "7dtd"
      roles:
        - ansible_role_7dtd

License
-------

MIT

Links
------------------

[https://7daystodie.fandom.com/wiki/Server](https://7daystodie.fandom.com/wiki/Server)
